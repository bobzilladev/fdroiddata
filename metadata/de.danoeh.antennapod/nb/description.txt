'''N.B.''' Flattr-funksjonaliteten er avskrudd i denne byggversjonen av programmet, siden API-nøklene ikke er i kildekoden.

Sjekk ut [https://github.com/AntennaPod/AntennaPod/wiki/About-AntennaPod#all-features hele funksjonslisten] på vår wiki.

